import './style.css';
import 'leaflet/dist/leaflet.css';
import L, { LeafletEvent, TileLayerOptions } from 'leaflet';
import { test } from './test';
import { Coordinates } from './MapTools/Coordinates';
import { NHSMarker } from './MapTools/ExtendedMarker';

L.Icon.Default.imagePath = 'img/icon/';

const defaultTilesPath =
    'https://static.xam.nu/dayz/maps/nhchernobyl/Mar.13/topographic/{z}/{x}/{y}.webp';

const defaultOptions = {
    tileSize: 256,
    noWrap: true,
    updateWhenZooming: !0,
    updateInterval: 200,
    zIndex: 1,
    maxZoom: 7,
    minZoom: 2,
    pane: 'tilePane',
    keepBuffer: 100,
    attribution: "Maptiles by <a href='http://xam.nu/' target='_blank'>xam.nu</a>, under CC BY.",
};

// const map = L.map('map', {
//     crs: L.CRS.Simple,
//     center: [-128, 128],
//     zoom: 3,
//     zoomControl: true,
//     layers: [m_mono],
// });

// const wm = new Coordinates({ position: 'bottomleft' });
// wm.addTo(map);

// map.addEventListener('mousemove', (event) => {
//     let latln = getCoords(event.latlng);
//     wm.updateHTML(latln.x, latln.y);
// });

// L.control
//     .scale({
//         imperial: false,
//         maxWidth: 300,
//     })
//     .addTo(map);

function getCoords(t: { lat: number; lng: number }): { x: number; y: number } {
    return {
        x: +((t.lng * 20480) / 256).toFixed(2),
        y: +(((256 + t.lat) * 20480) / 256).toFixed(2),
    };
}

function H(x: number, y: number) {
    return {
        lat: (256 * y) / 20480 - 256,
        lng: (256 * x) / 20480,
    };
}

// test.CacheList.forEach((clIt) => {
//     clIt.Coords.forEach((coord) => {
//         console.log([coord.Position[0], coord.Position[2]]);
//         const cd = H(coord.Position[0], coord.Position[2]);
//         const marker = L.marker([cd.lat, cd.lng]);
//         marker.addTo(map);
//     });
// });

function createTileLayer(path?: string, options?: TileLayerOptions) {
    return L.tileLayer(path ?? defaultTilesPath, options ?? defaultOptions);
}

export function createMap(container: string | HTMLElement) {
    const tileLayer = createTileLayer();
    return L.map(container, {
        crs: L.CRS.Simple,
        center: [-128, 128],
        zoom: 0,
        zoomControl: true,
        layers: [tileLayer],
    });
}

export function toClickablerMarker<T = undefined>(
    coords: (obj: any) => { lat: number; lng: number },
    obj: T,
    onclick: (obj: T, e: LeafletEvent) => void
) {
    const cd = coords(obj);
    const marker = new NHSMarker([cd.lat, cd.lng]);
    marker.on('click', (e: LeafletEvent) => {
        marker.bindPopup(JSON.stringify(obj));
    });
}
