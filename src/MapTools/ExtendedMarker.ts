import L, { LatLngExpression } from "leaflet";

export class NHSMarker<T> extends L.Marker {
    declare options: L.MarkerOptions;

    constructor(latLng: LatLngExpression, options?: L.MarkerOptions) {
        super(latLng, options);
    }
}