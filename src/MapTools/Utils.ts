export function getXY(t: { lat: number; lng: number }): { x: number; y: number } {
    return {
        x: +((t.lng * 20480) / 256).toFixed(2),
        y: +(((256 + t.lat) * 20480) / 256).toFixed(2),
    };
}

export function getLatLng(x: number, y: number) {
    return {
        lat: (256 * y) / 20480 - 256,
        lng: (256 * x) / 20480,
    };
}