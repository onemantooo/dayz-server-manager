import L from "leaflet";

const text = L.DomUtil.create('div');

export const Coordinates = L.Control.extend({
    onAdd: function(map) {
        text.innerText = 'Move cursor to view position';
        text.style.background = '#38f';
        text.style.padding = '5px';

        return text;
    },

    updateHTML: function(x: number, y: number) {
        text.innerText = `X: ${x}, Y: ${y}`
      }
});
