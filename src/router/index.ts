import { createRouter, createWebHistory, RouteRecordRaw } from 'vue-router';

const routes: Array<RouteRecordRaw> = [
    {
        path: '/',
        redirect: {
            name: 'Map',
        },
    },
    {
        path: '/map',
        name: 'Map',
        props: (route) => ({
            ...route.query,
        }),
        component: () => import(/* webpackChunkName: "demo" */ '../App/Pages/Map/Map.vue'),
    },
];

const router = createRouter({
    history: createWebHistory(),
    routes,
});

export default router;
