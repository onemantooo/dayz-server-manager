const imageMapTiles = require('image-map-tiles');

var options = {
    'outputDir': 'public/tiles',
    'zoom': 5,
    'tileHeight': 256,
    'tileWidth': 256
}

imageMapTiles('path/to/image', options );